# -*- coding: utf-8 -*-
"""
Created on Mon May 18 11:35:24 2020

@author: diego
"""
import serial #Import the serial library
from flask import Flask, request, Response, render_template
from flask_cors import CORS, cross_origin
from json import dumps
app = Flask(__name__)

@app.route('/')
def hello_w():
    return app.send_static_file('index.html')

serData = serial.Serial('com3', 9600)
ans = 1
@app.route('/get_ser', methods=['GET'])
def monitor():
    global ans
    #print(ans)
    if serData.inWaiting() > 0:
        myData = int(serData.readline())
        ans = myData
    else:
        myData = ans
        #print('this' + str(ans))
    #print(myData)
    return Response(dumps({'content': myData}), mimetype='application/json')

@app.route('/set_ser', methods=['POST'])
def emergency():
    x = request.get_json()
    x = x['emergency'].encode('utf-8')
    serData.write((x))
    print(x)
    global ans
    #print(ans)
    if serData.inWaiting() > 0:
        myData = int(serData.readline())
        ans = myData
    else:
        myData = ans
    return Response(dumps({'content': myData}), mimetype='application/json')

@app.route('/walk_ser', methods=['POST'])
def walk():
    x = request.get_json()
    x = x['walker'].encode('utf-8')
    serData.write((x))
    print(x)
    global ans
    #print(ans)
    if serData.inWaiting() > 0:
        myData = int(serData.readline())
        ans = myData
    else:
        myData = ans
    return Response(dumps({'content': myData}), mimetype='application/json')

if __name__ == '__main__':
    app.run(port = 8080)
