# -*- coding: utf-8 -*-
"""
Created on Sun May 17 20:30:21 2020

@author: diego
"""
import kivy
from kivy.app import App

from kivy.uix.label import Label


class MyApp(App):
    def build(self):
        return Label(text='Diego')
    
    
if __name__ == '__main__':
    MyApp().run()