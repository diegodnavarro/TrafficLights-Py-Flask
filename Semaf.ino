const int g = 3;
const int y = 4;
const int r = 5;
bool state = false;
int gr = true;
int ye = false;
int re = false;
void setup() {
  Serial.begin(9600);
  pinMode(g, OUTPUT);
  pinMode(y, OUTPUT);
  pinMode(r, OUTPUT);
  pinMode(7, OUTPUT);
}

void loop() {
  green();
  greenBlink();
  yellow();
  red();

}

void green() {
  Serial.println(1);
  digitalWrite(g, HIGH);
  digitalWrite(y, LOW);
  digitalWrite(r, LOW);
  for (int i = 0; i < 14; i++) {
    checkemergency();
    delay(500);
  }
}

void greenBlink() {
  for (int z = 0; z < 3; z++) {
    digitalWrite(g, HIGH);
    digitalWrite(y, LOW);
    digitalWrite(r, LOW);
    Serial.println(1);
    delay(500);

    digitalWrite(g, LOW);
    digitalWrite(y, LOW);
    digitalWrite(r, LOW);
    Serial.println(0);
    delay(500);
  }
}

void yellow() {
  Serial.println(2);
  digitalWrite(g, LOW);
  digitalWrite(y, HIGH);
  digitalWrite(r, LOW);
  digitalWrite(7, LOW);
  for (int i = 0; i < 4; i++) {
    checkemergency();
    delay(500);
  }
}

void red() {
  Serial.println(3);
  digitalWrite(g, LOW);
  digitalWrite(y, LOW);
  digitalWrite(r, HIGH);
  for (int i = 0; i < 14; i++) {
    checkemergency();
    delay(500);
  }
}

void checkemergency() {
  int ser = 0;
  if (Serial.available() > 0) {
    ser = (Serial.parseInt());
    Serial.println(ser);
    delay(50);
    if ( ser == 1) {
      if (state == false) {
        Serial.println(5);
        while (state == false) {
          digitalWrite(g, HIGH);
          digitalWrite(y, HIGH);
          digitalWrite(r, HIGH);
        }
      }
    } else if (ser > 3) {
      digitalWrite(7, HIGH);
      greenBlink();
      yellow();
      red();
      loop();
    }
  }
  Serial.flush();
}
